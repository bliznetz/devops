#!/bin/bash
yc compute instance create \
--name $1 \
--hostname $1 \
--ssh-key ~/.ssh/id_rsa.pub \
--metadata key1=data1,key2=data2 \
--create-boot-disk image-folder-id=standard-images,image-family=ubuntu-2004-lts \
--zone ru-central1-a \
--network-interface subnet-name=default-ru-central1-a,nat-ip-version=ipv4
