import urllib.request


def handler(event, context):
    contents = urllib.request.urlopen("http://wttr.in/led").read().decode()
    return {
        'statusCode': 200,
        'body': f'<html>Current weather in SPB: {contents}</html>',
    } 

if __name__=="__main__":
    print(handler("",""))


