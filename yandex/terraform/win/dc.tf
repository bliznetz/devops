terraform {
  required_providers {
    virtualbox = {
      source = "terra-farm/virtualbox"
      version = "0.2.2-alpha.1"
    }
  }
}

# There are currently no configuration options for the provider itself.

resource "virtualbox_vm" "tfdc" {
  name      = "tfdc"
  image     = "../windcwinrm.ova"
  cpus      = 2
  memory    = "512 mib"
 # user_data = file("${path.module}/user_data")

  network_adapter {
    type           = "hostonly"
    host_interface = "vboxnet0"
  }
}

resource "virtualbox_vm" "client1" {
  name      = "winclient1"
  image     = "../win10.ova"
  cpus      = 2
  memory    = "1024 mib"
  user_data = file("${path.module}/user_data")

  network_adapter {
    type           = "hostonly"
    host_interface = "vboxnet0"
  }
}

resource "virtualbox_vm" "client2" {
 name      = "winclient2"
 image     = "../win10.ova"
 cpus      = 2
 memory    = "512 mib"
 user_data = file("${path.module}/user_data")

 network_adapter {
   type           = "hostonly"
   host_interface = "vboxnet0"
 }
}

data "template_file" "inventory" {
  template = "${file("${path.module}/inventory.tpl")}"
  vars = {
    IPAddr-dc = element(virtualbox_vm.tfdc.*.network_adapter.0.ipv4_address, 1)
    IPAddr-c1 = element(virtualbox_vm.client1.*.network_adapter.0.ipv4_address, 1)
    IPAddr-c2 = element(virtualbox_vm.client2.*.network_adapter.0.ipv4_address, 1)
  }
}
output "template" {
  value = data.template_file.inventory.rendered
}

resource "local_file" "rendered-inventory" {
  content = "${data.template_file.inventory.rendered}"
  filename = "ansible/rendered-inventory"
}

resource "time_sleep" "waitforWinrm" {
  depends_on = [ virtualbox_vm.client1, virtualbox_vm.client2, virtualbox_vm.tfdc ]
  create_duration = "180s"
}

resource "null_resource" "ansibleExec" {
  depends_on = [ time_sleep.waitforWinrm ]
  provisioner "local-exec" {
  command = "(cd ansible ;  ansible-playbook -i rendered-inventory postinstall.yml )"
  }
}
