[firstdc]
tfdc ansible_host=${IPAddr-dc} ansible_user=Administrator ansible_password=password1 

[clients]
client1 ansible_host=${IPAddr-c1}
client2 ansible_host=${IPAddr-c2}
[clients:vars]
ansible_user=Admin
ansible_password=RandomPassword

[win:children]
firstdc
clients
[win:vars]
ansible_port=5985 
ansible_user=Administrator 
ansible_connection=winrm 
ansible_winrm_transport=basic 
ansible_winrm_server_cert_validation=ignore
