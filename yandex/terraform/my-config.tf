 terraform {
   required_providers {
     yandex = {
       source = "yandex-cloud/yandex"
     }
   }
 }
  
 provider "yandex" {
  # YC_TOKEN=$(yc iam create-token)
   token  =  "t1.9euelZqXlceWkZqKmYrGx43NyJbNiu3rnpWaxorHm56VzoqYmcuWys7MmIrl8_c_J0pP-e8yODJV_d3z939VR0_57zI4MlX9zef1656VmpuYnsnGnIvNicqemZ2Ynpia7_zF656VmpuYnsnGnIvNicqemZ2Ynpia.w8XKb7qR0Q58SpBQokZlhHygwCQx0BxVfez1vm0wH4xdf3a0uaZ0tcpLczkxZii84STYgIvpF9cM0ABOwttnCA"
   cloud_id  = "b1ga3oivri0p9cuc2kig"
   folder_id = "b1gcj7eqn9h7uqmohn5e"
   zone      = "ru-central1-a"
 }
 

variable "image-id" {
  type = string
}
 
resource "yandex_compute_instance" "vm-1" {
  name = "from-terraform-vm"
  platform_id = "standard-v1"
  zone = "ru-central1-a"
 
  resources {
    cores  = 2
    memory = 2
  }
 
  boot_disk {
    initialize_params {
      image_id = var.image-id
    }
  }
 
  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }
 
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}
 
resource "yandex_vpc_network" "network-1" {
  name = "from-terraform-network"
}
 
resource "yandex_vpc_subnet" "subnet-1" {
  name           = "from-terraform-subnet"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.network-1.id}"
  v4_cidr_blocks = ["10.2.0.0/16"]
}
 
#--------------

#------terraform state list ----------

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}
 
output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

# ------------
