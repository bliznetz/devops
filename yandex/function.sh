#!/bin/bash

# Service account creation
export SERVICE_ACCOUNT=$(\
yc iam service-account create\
    --name service-account-for-cf\
    --description "service account for cloud functions"\
    --format json | jq -r .) 

echo Test
yc iam service-account list 
echo $SERVICE_ACCOUNT

echo Getting service account ID
export SERVICE_ACCOUNT_ID=$(yc iam service-account list | grep "service-account-for-cf" | awk  '{print $2}')
echo  "export SERVICE_ACCOUNT_ID=$SERVICE_ACCOUNT_ID" >> ~/.bashrc && . ~/.bashrc 
echo $SERVICE_ACCOUNT_ID 

echo Getting folder IG
echo "export FOLDER_ID=$(yc config get folder-id)" >> ~/.bashrc && . ~/.bashrc  
echo $FOLDER_ID 

echo Granting Editor role to service account
yc resource-manager folder add-access-binding $FOLDER_ID\
    --subject serviceAccount:$SERVICE_ACCOUNT_ID\
    --role editor

echo Creating function
yc serverless function create --name itmo-function

echo Creating version
yc serverless function version create\
    --function-name itmo-function\
    --memory 256m\
    --execution-timeout 20s\
    --runtime python37\
    --entrypoint main.handler\
    --service-account-id $SERVICE_ACCOUNT_ID\
    --source-path py/main.py

echo Function and versions list
yc serverless function list 
yc serverless function version list --function-name itmo-function

echo invocation test

yc serverless function invoke $(yc serverless function list | grep itmo-function | awk '{print $2}')

echo making function public
yc serverless function allow-unauthenticated-invoke itmo-function

echo getting function URL
echo
echo
yc serverless function get itmo-function | grep http_invoke_url
echo
