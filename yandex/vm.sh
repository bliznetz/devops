yc compute instance create \
  --name itmo-test-2 \
  --zone ru-central1-d \
  --network-interface subnet-name=zone-d,nat-ip-version=ipv4 \
  --create-boot-disk image-folder-id=standard-images,image-family=ubuntu-2204-lts  \
  --metadata-from-file user-data=startup.sh 
#  --ssh-key ~/.ssh/spring24.pub
