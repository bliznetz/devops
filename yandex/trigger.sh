#!/bin/bash

export BUCKET_NAME="itmo-trigger"

. ~/.bashrc
echo "check: FOLDER_ID = $FOLDER_ID and SERVICE_ACCOUNT_ID = $SERVICE_ACCOUNT_ID"

echo Adding storage.editor role to service account

yc resource-manager folder add-access-binding $FOLDER_ID \
    --role storage.editor \
    --subject serviceAccount:$SERVICE_ACCOUNT_ID 
echo Creating access key
#
#yc iam access-key create --service-account-name service-account-for-cf | \
#    awk '/key_id/{print "export ACCESS_KEY="$2} /secret/{print "export SECRET_KEY="$2}' >> ~/.ya_keys && chmod a+x ~/.ya_keys &&. ~/.ya_keys

chmod a+x ~/.ya_keys &&. ~/.ya_keys

cd trigger
zip function.zip main.py requirements.txt 
cd ..

# echo $ACCESS_KEY $SECRET_KEY

yc serverless function version create \
  --function-name itmo-function \
  --memory 256m \
  --execution-timeout 5s \
  --runtime python37 \
  --entrypoint main.handler \
  --service-account-id $SERVICE_ACCOUNT_ID \
  --environment ACCESS_KEY=$ACCESS_KEY \
  --environment SECRET_KEY=$SECRET_KEY \
  --environment BUCKET_NAME=$BUCKET_NAME \
  --source-path trigger/function.zip 

yc serverless trigger create object-storage \
  --name my-first-trigger \
  --bucket-id $BUCKET_NAME \
  --events 'create-object' \
  --prefix 'input' \
  --invoke-function-name itmo-function \
  --invoke-function-service-account-id $SERVICE_ACCOUNT_ID 



echo getting function URL
echo
echo
yc serverless function get itmo-function | grep http_invoke_url
echo
